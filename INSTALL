install.sh is a POSIX sh script known to work with the following shells:

  ash
  dash
  bash
  ksh
  loksh
  mksh
  pdksh
  posh (0.14.1 or newer)
  oksh
  yash
  zsh

install.sh accepts the following arguments as targets:

build - Generates the solarus.data archive and the executable script.
clean - Removes the solarus.data archive and the executable script.
install - Installs the solarus.data archive and executable script.
install-assets - Installs the docs, icons and desktop file.
uninstall - Uninstalls all installed files.

The build, clean and install targets can customize the name of the data
archive.

  DATANAME - Name of the solarus data archive (data.solarus)

Additionally the following arguments can be passed to the install
and uninstall targets to configure the install paths.

  PREFIX - Install path prefix (/usr/local)
  BINDIR - Binary install directory (PREFIX/bin)
  DOCDIR - Documentation install directory (PREFIX/share/doc)
  MANDIR - Manpage install directory (PREFIX/share/man)
  DATADIR - Read-only data install directory (DATAROOTDIR)
  DATAROOTDIR -  Read-only data root install directory (PREFIX/share)
  SYSCONFDIR - Read-only data install directory (PREFIX/etc)

The configurable install directories can be either absolute or relative
paths. If they are set as relative paths the value of PREFIX will be
prepended to the install directory. For example if MANDIR is set to
'man' man pages will be installed to '/usr/local/man/' instead of
'/usr/local/share/man/'.

By design install.sh will not use any set environment variables and all
configuration must be passed explicitly as arguments.

EXAMPLES

  ./install.sh install PREFIX=/usr DESTDIR=/tmp

      Installs to '/tmp/usr/'.

  ./install.sh uninstall PREFIX=/usr DESTDIR=/tmp

      Uninstalls from '/tmp/usr/'.

  ./install.sh install DOCDIR=/usr/local/doc

      Installs documentation to '/usr/local/doc/$PRGNAM-$VERSION'.

  ./install.sh install MANDIR=man

      Installs man pages to '$PREFIX/man/man1/'.

  ./install.sh install SYSCONFDIR=/etc

      Installs read-only data to '/etc/'.
